package com.javainuse.controllers;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javainuse.model.Employee;

@RestController
public class DemoController {

	private boolean breakCircuit = true;

	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	@HystrixCommand(fallbackMethod = "getDataFallBack")
	public Employee getEmployeeDetails() {
		breakCircuit = false;
		System.out.println("Processing Client Call - Inside method getEmployeeDetails()");
		Employee emp = new Employee();
		emp.setName("Pradeep G T");
		emp.setDesignation("Developer");
		emp.setEmpId("123");
		emp.setSalary(3000);

		if(emp.getName().equalsIgnoreCase("Pradeep G T"))
			throw new RuntimeException();

		return emp;
	}

	public Employee getDataFallBack() {
		if(breakCircuit)
			System.out.println("After Several Retries - Hystrix Circuit Break Triggered ");
		else
			System.out.println("Exception Occurred (Fallback method)");

		Employee emp = new Employee();
		emp.setName("fallback-emp1");
		emp.setDesignation("fallback-manager");
		emp.setEmpId("fallback-1");
		emp.setSalary(2000);
		breakCircuit= true;
		return emp;
	}
}